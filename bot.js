/*
Jim Woodyard bot
@WoodyardBot

Created by Edwin Finch
*/

var FIX = false;

var PRODUCTION = true;
var GROUP_CHAT_ID;
var BOT_USERNAME;

var TelegramBot = require('node-telegram-bot-api');
var fs = require('fs');
var plotly = require('plotly')('edwinfinch','zjoygft1ot');
var urban = require('urban');
var analytics = require('./chat_analytics');
var charts = require('./charts');

//yolo
String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

var token;
if(PRODUCTION){
    //@WoodyardBot
    console.log("You are running a production bot!");
    token = '174114726:AAEqftQ_h4MwHyOsIgzgk_52p6_cOTK4_I0';
    console.log(token);
    GROUP_CHAT_ID = -12880731;
    BOT_USERNAME = "WoodyardBot";
}
else{
    //@WoodyardSandboxBot
    console.log("You are running a SANDBOX bot!");
    token = "159148541:AAHJuEO4fUS2ZoWwhm-0hn_K50XtesDyNw8";
    console.log(token);
    GROUP_CHAT_ID = -39641397;
    BOT_USERNAME = "WoodyardSandboxBot";
}
var PHOTO_DIR = "images/";
var bot = new TelegramBot(token, {polling: true});

var bannedUsers = {

};
var turnOnDate;
var lookingUp;
var COMMAND_LIST_STATIC = undefined;
var COMMAND_LIST_DETAILS = undefined;
var globally_banned = false;

function findCommandObjectIndex(command){
    for(index in COMMAND_LIST_STATIC.commands){
        var commandObject = COMMAND_LIST_DETAILS.commands[index];
        if(commandObject.command.name === command){
            return index;
        }
    }
    return -1;
}

function findCommandObject(command){
    var index = findCommandObjectIndex(command);
    if(index != -1){
        return COMMAND_LIST_DETAILS.commands[index];
    }
    return undefined;
}

function refuse(command, msg){
    if(!PRODUCTION){
        return false;
    }
    if(msg.forward_from){
         if(msg.forward_from.username === BOT_USERNAME){
             return true;
         }
    }
    console.log("Searching for " + command);
    var actualCommandObject = findCommandObject(command);

    console.log(JSON.stringify(actualCommandObject));

    if(actualCommandObject.banned){
        console.log(command + " is banned.");
        return true;
    }
    if(!actualCommandObject){
        console.log("Didn't find " + command);
        return true;
    }
    if(msg.chat.type === "group" && (!actualCommandObject.group_enabled)){
        bot.sendMessage(msg.chat.id, "This command is not supported in group chats.", { reply_to_message_id:msg.message_id });
        return true;
    }
    return false;
}

function currentlyTyping(msg){
    bot.sendChatAction(msg.chat.id, "typing");
}

function reloadData(msg){
    fs.readFile("resources/commands.json", 'utf8', function(err, data) {
        if(err){
            console.error("ERROR OPENING COMMANDS " + err);
            return;
        }
        COMMAND_LIST_STATIC = JSON.parse(data);
        COMMAND_LIST_DETAILS = COMMAND_LIST_STATIC;
        console.log("Commands are now enabled.");
    	if(msg){
	    bot.sendMessage(msg.chat.id, "Reloaded.", {reply_to_message_id:msg.message_id});
	}
    });
}

function hourChanged(callback) {
    if(PRODUCTION){
        bot.sendChatAction(GROUP_CHAT_ID, "upload_photo");

        charts.renderDaysChart(function(result){
            if(!result){
                console.log("Success rendering days chart.");
                bot.sendMessage(1494633, "Success rendering days chart!");
            }
            else{
                bot.sendMessage(1494633, "Error rendering days chart: " + JSON.stringify(result));
            }
            charts.renderHoursChart(function(result1){
                if(!result1){
                    console.log("Success rendering hours chart.");
                    bot.sendMessage(1494633, "Success rendering hours chart!");
                }
                else{
                    bot.sendMessage(1494633, "Error rendering hours chart: " + JSON.stringify(result1));
                }
                if(callback){
                    callback();
                }
                if((new Date()).getHours() === 23){
                    setTimeout(function(){
                        bot.sendPhoto(GROUP_CHAT_ID, 'graphs/days.png');
                        setTimeout(function(){
                            bot.sendPhoto(GROUP_CHAT_ID, 'graphs/hours.png');
                            setTimeout(function(){
                                bot.sendMessage(GROUP_CHAT_ID, "Daily stats updated and delivered to your doorstep automagically");
                            }, 1000);
                        }, 500);
                    }, 250);
                }
            });
        });
    }
    else{
        if(callback){
            setTimeout(function(){
                callback();
            }, 1000);
        }
        console.log("Hour changed");
    }
}

bot.getMe().then(function (me) {
    console.log('Hello, my name is %s and I am alive! Commands are turned off for while I load...', me.username);

    reloadData(undefined);

    setInterval(function timeChecker() {
        hourChanged();
    }, PRODUCTION ? (60*60*1000) : (60000));
    hourChanged();
});

bot.onText(/\/reload/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        return;
    }
    reloadData(msg);
});

bot.onText(/\/graphreload/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        return;
    }
    bot.sendMessage(msg.chat.id, "Reloading graphs...");
    hourChanged(function(){
        bot.sendMessage(msg.chat.id, "Reloaded!");
    });
});

//uncomment this and then rerun the bot and wait a few seconds then close the bot and comment this out again and then rerun it and it
//will fix any issues you have with messages overflowing

if(FIX){
    console.log("FIX is true, refusing to register commands");
    return;
}

bot.onText(/\/daily/, function(msg, match){
    if(refuse("daily", msg)){ return; }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/days.png');
});

bot.onText(/\/hourly/, function(msg, match){
    if(refuse("hourly", msg)){ return; }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    bot.sendPhoto(msg.chat.id, 'graphs/hours.png');
});

bot.onText(/\/lit/, function(msg, match){
    if(refuse("lit", msg)){ return; }

    var currentTime = Math.floor(Date.now()/1000);

    var howLitData = {
        secondly: 0,
        minutely: 0,
        hourly: 0,
        daily: 0
    };

    analytics.getCountForSearch({ "message.date":{"$gt":currentTime-86400, "$lt":currentTime} }, 0, function(data){
        howLitData.daily = data.count;
        analytics.getCountForSearch({ "message.date":{"$gt":currentTime-3600, "$lt":currentTime} }, 0, function(data){
            howLitData.hourly = data.count;
            analytics.getCountForSearch({ "message.date":{"$gt":currentTime-60, "$lt":currentTime} }, 0, function(data){
                howLitData.minutely = data.count;
                howLitData.secondly = (data.count/60).toFixed(2);

                var litString = "";
                var totallyLit = false;
                if(howLitData.minutely >= 30){
                    litString = "FIRE FIRE FIRE FIRE\nCHAT IS TOTALLY FUCKING LIT\n\n";
                    totallyLit = true;
                }
                else if(howLitData.minutely >= 20){
                    litString = "Holy shit, group chat is LIT\n\n";
                }
                else if(howLitData.minutely > 8){
                    litString = "Not bad, group chat is sort of lit\n\n";
                }
                else{
                    litString = "Chat's not lit, flame has been stomped on by VM\n\n";
                }
                litString += howLitData.secondly + " per second\n";
                litString += howLitData.minutely + " per minute\n";
                litString += howLitData.hourly + " per hour\n";
                litString += howLitData.daily + " per day\n\n";
                if(totallyLit){
                    litString = litString.toUpperCase();
                }
                bot.sendMessage(msg.chat.id, litString);
            });
        });
    });
});

bot.onText(/\/testread/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        return;
    }
    analytics.readFromDatabase({"test":true}, function(error, data){
        if(!error){
            bot.sendMessage(msg.chat.id, "Success reading.", {reply_to_message_id:msg.message_id});
        }
        else{
            bot.sendMessage(msg.chat.id, "Failed reading. Error: " + error, {reply_to_message_id:msg.message_id});
        }
    });
});

bot.onText(/\/testwrite/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        return;
    }
    analytics.writeToDatabase([{ "time":((new Date()).getTime()), "test":true }, { "hello":((new Date()).getTime()) }], function(error, data){
        if(!error){
            bot.sendMessage(msg.chat.id, "Success writing.", {reply_to_message_id:msg.message_id});
        }
        else{
            bot.sendMessage(msg.chat.id, "Failed writing. Error: " + error, {reply_to_message_id:msg.message_id});
        }
    });
});

bot.onText(/\/tldr (.+)/, function(msg, match){
    if(refuse("tldr", msg)){ return; }

    if(msg.chat.type === "group"){
        bot.sendMessage(msg.chat.id, "To prevent spamming, please message me for a TL;DR privately", {reply_to_message_id:msg.message_id});
        return;
    }

    var spaceLocation = match[1].indexOf(" ");
    var number = parseInt(match[1].substring(spaceLocation+1));
    if(!number){
        bot.sendMessage(msg.chat.id, "Invalid format, please enter in format:\n/tldr last 60", {reply_to_message_id:msg.message_id});
        return;
    }

    var totalCount;
    analytics.getTotalCount(function(count){
        totalCount = count;

        if(number > (totalCount-1)){
            bot.sendMessage(msg.chat.id, "Sorry, you can't request more than " + (totalCount-1) + " messages.", {reply_to_message_id:msg.message_id});
            return;
        }

        analytics.getTLDRData(totalCount, number, function(error, docs){
            if(error){
                bot.sendMessage(msg.chat.id, "Windows error: " + error + ", sorry.", {reply_to_message_id:msg.message_id});
                return;
            }
            var tldr_string = "Begin TL;DR last " + number + ": \n\n";
            var tldr_docs = [];
            var amountToGrab = Math.ceil(number*0.1);
            var sectionGrabAmount = Math.ceil(amountToGrab*0.1);
            console.log("Amount to grab " + amountToGrab + " and sectionGrabAmount " + sectionGrabAmount + " and docs " + docs.length);
            if(sectionGrabAmount < 1){
                sectionGrabAmount = 1;
            }
            for(var i = 0; i < 10; i++){
                for(var i1 = 0; i1 < sectionGrabAmount; i1++){
                    var arrayLocation = (i*amountToGrab) + (i1*Math.floor(Math.random() * sectionGrabAmount+1));
                    if(arrayLocation < docs.length){
                        tldr_docs.push(docs[arrayLocation]);
                    }
                }
            }
            for(var i = 0; i < tldr_docs.length; i++){
                var textToSend = tldr_docs[i].message.text;
                if(textToSend){
                    if(tldr_docs[i].message.forward_from){
                        textToSend = "[forwarded message from " + tldr_docs[i].message.forward_from.first_name + "]";
                    }
                    var date = new Date(parseInt(tldr_docs[i].message.date)*1000);
                    var dateString = date.toTimeString().substring(0, 5);
                    tldr_string += tldr_docs[i].message.from.first_name + " @ " + dateString + ": '" + textToSend + "'\n";
                }
            }
            if(tldr_string.length > 3900){
                var amountOfMessages = Math.ceil(tldr_string.length/3900);
                for(var i = 0; i < amountOfMessages; i++){
                    var message = tldr_string.substring(3900*i, 3900*(i+1));
                    bot.sendMessage(msg.chat.id, "Part " + (i+1) + ":\n\n" + message, {reply_to_message_id:msg.message_id});
                }
            }
            else{
                tldr_string += "---\nDisclaimer: TL;DRs are made up of 10% of the last N messages you chose. Please keep in mind that as you expand N, the gap between messages" +
                " becomes signifigantly larger and they make less and less sense.";
                bot.sendMessage(msg.chat.id, tldr_string, {reply_to_message_id:msg.message_id});
            }
        });
    });
});

bot.onText(/\/totalsent/, function(msg, match){
    if(refuse("totalsent", msg)){ return; }

    analytics.getTotalCount(function(count){
        if(count){
            bot.sendMessage(msg.chat.id, count + " messages have been sent in total since December 13th 14:00, 2015.");
        }
    });
});

function getLurkerString(percentage, first_name){
    var lurkerString = "";
    if(percentage > 15){
        lurkerString += "❎ " + first_name + " is not a lurker ❎\n\nYou are an anti-lurker. You literally contribute to the group chat so much that it consumes you. You leave notifications on for the group chat and respond to any chirp made to you with a 3 message long essay. Maybe it's time to take a break.";
    }
    else if(percentage > 7 && percentage <= 15){
        lurkerString += "❎ " + first_name + " is not a lurker ❎\n\nYou are a good group chat member. You contribute daily to the group chat, from sharing anger about Star Wars The Force Awakens to what game you're gonna play tonight.";
    }
    else if(percentage >= 5 && percentage <= 7){
        lurkerString += "😶 " + first_name + " is close to a lurker 😶\n\nYou are on the verge of being a lurker 😥. Make sure to keep posting dank memes daily and let people know how much ram you've purchased. You'll be ok. If you don't, you could end up as a top lurker and be banished to the outlands.";
    }
    else if(percentage < 5 && percentage > 2){
        lurkerString += "✅ " + first_name + " is a lurker ✅\n\nYou are a lurker 😶. Although you do not contribute much to the group, there is still hope for you down the road. Make sure to contribute outside of your comfort zone and you won't end up as an ultimate lurker.";
    }
    else if(percentage < 3 && percentage > 0){
        lurkerString += "✅ " + first_name + " is a HARDCORE lurker ✅\n\nYou are a hardcore lurker 😢. You have barely contribute to the group, and when you do, it's shitty 9-gag reposts full of shit that nobody cares about. At this rate, you might as well give up.";
    }
    else{
        lurkerString += "✅ " + first_name + " is an ULTIMATE lurker ✅\n\nYou are one of the top lurkers. You have not contributed anything to the group and should probably just pull the trigger. There's barely any hope for you to get your lurker status to the famous 'anti-lurker'. It's literally in your fucking Genes.";
    }
    return lurkerString;
}

bot.onText(/\/lurkerstatus/, function(msg, match){
    if(refuse("lurkerstatus", msg)){ return; }

    analytics.getTotalCount(function(totalCount){
        analytics.getUserStats(msg.from.id, function(stats){
            if(stats){
                var percentage = Math.floor((stats.sent/totalCount)*100);
                var lurkerString = getLurkerString(percentage, msg.from.first_name);

                bot.sendMessage(msg.chat.id, lurkerString, {reply_to_message_id:msg.message_id});
            }
            else{
                bot.sendMessage(msg.chat.id, "I couldn't fetch that user's stats. Are you sure they exist?");
            }
        });
    });
});

bot.onText(/\/mystats/, function(msg, match){
    if(refuse("mystats", msg)){ return; }

    analytics.getTotalCount(function(totalCount){
        analytics.getUserStats(msg.from.id, function(stats){
            if(stats){
                var statString = "Stats for " + msg.from.first_name + ":\n";
                statString += "Sent " + stats.sent + " messages to the group chat.\n";
                statString += msg.from.first_name + " accounts for about " + Math.floor((stats.sent/totalCount)*100) + "% of the group chat messages.\n";
                statString += "Is Telegram user " + msg.from.id + ".\n";
                statString += "\nPlease note: All stats are from December 13th, 2015 and on.";

                bot.sendMessage(msg.chat.id, statString, {reply_to_message_id:msg.message_id});
            }
            else{
                bot.sendMessage(msg.chat.id, "I couldn't fetch that user's stats. Are you sure they exist?");
            }
        });
    });
});

bot.onText(/\/306/, function(msg, match){
    if(refuse("306", msg)){ return; }

    bot.forwardMessage(msg.chat.id, 1494633, 1166);
});

bot.onText(/\gover2/, function(msg, match){
    if(refuse("gover2", msg)){ return; }

    bot.forwardMessage(msg.chat.id, 1494633, 1167);
});

bot.onText(/\/off (.+)/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to press the big red button.", {reply_to_message_id:msg.message_id});
        return;
    }

    var colonIndex = match[1].indexOf(":");
    var minutes = parseInt(match[1].substring(colonIndex+1));
    var commandString = match[1].substring(0, colonIndex);
    var commandObjectIndex = findCommandObjectIndex(commandString);
    var commandObject = findCommandObject(commandString);
    commandObject.banned = true;
    COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;

    bot.sendMessage(msg.chat.id, "Disabled. Command '/" + commandObject.command.name + "' is now shutdown for " + minutes + " minutes.");

    setTimeout(function(){
        commandObject.banned = false;
        COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;
        console.log(commandObject.command.name + " command is now re-enabled.");
    }, (minutes*60*1000));
});

bot.onText(/\/on (.+)/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to press the big green button.", {reply_to_message_id:msg.message_id});
        return;
    }

    var commandString = match[1];
    var commandObjectIndex = findCommandObjectIndex(commandString);
    var commandObject = findCommandObject(commandString);
    commandObject.banned = false;
    COMMAND_LIST_DETAILS.commands[commandObjectIndex] = commandObject;

    bot.sendMessage(msg.chat.id, "Command '/" + commandString + "' is now completely enabled.");
});

function handleReply(msg){
    switch(msg.reply_to_message.text){
        case "What period? (1-4)":
            provideExamDate(msg);
            break;
    }
}

var lastID = 0;

// Any kind of message
bot.on('message', function (msg) {
    if(msg.date === lastID){
    	console.log("Windows error");
    	return;
    }
    lastID = msg.date;

    console.log(JSON.stringify(msg));
    ////console.log("Message ID: " + msg.message_id + " and chat ID: " + msg.chat.id);

    if(PRODUCTION){
        if(msg.chat.id === GROUP_CHAT_ID && msg.chat.type === "group"){
            analytics.writeToDatabase(false, {"message":msg});
        }
    }

    if(msg.reply_to_message){
        //console.log("Is a reply...");
        if(msg.reply_to_message.from.username === BOT_USERNAME){
            //console.log("Is woodyard bot!");
            handleReply(msg);
            return;
        }
    }

    if(lookingUp && msg.from.username === "Lignite" && msg.forward_from !== undefined){
        analytics.getTotalCount(function(totalCount){
            analytics.getUserStats(msg.forward_from.id, function(stats){
                if(stats){
                    var lookupString = "Lookup result: success";
                    lookupString += "\n\nmsg.forward_from: " + JSON.stringify(msg.forward_from);
                    var percentage = ((stats.sent/totalCount)*100);
                    var lurkerString = getLurkerString(Math.floor(percentage), msg.forward_from.first_name);
                    var stats = {
                        rawPercentage: percentage.toFixed(2),
                        userIsBanned: (bannedUsers['' + msg.forward_from.id] !== undefined),
                        lurkerStatus: lurkerString.substring(0, lurkerString.indexOf("\n\n"))
                    };
                    lookupString += "\ngroupstats: " + JSON.stringify(stats);

                    bot.sendMessage(msg.chat.id, lookupString, { reply_to_message_id:msg.message_id});
                    lookingUp = false;
                }
                else{

                }
            });
        });
        return;
    }

    if(msg.new_chat_participant){
        //console.log("New participant");
        currentlyTyping(msg);
        setTimeout(function(){
            if(msg.new_chat_participant.username === BOT_USERNAME){
                //console.log("I joined");
                bot.sendMessage(msg.chat.id, "Hello students, it's time to learn some physics. I'll be the best teacher you've ever had.");
            }
            else{
                //console.log("Someone else joined");
                bot.sendMessage(msg.chat.id, "Welcome " + msg.new_chat_participant.first_name + ". Ready to learn some physics?");
            }
        }, 1200);
    }
    else if(msg.left_chat_participant){
        //console.log("participant left");
        if(msg.left_chat_participant.username !== BOT_USERNAME){
            currentlyTyping(msg);
            setTimeout(function(){
                //console.log("Someone dropped");
                bot.sendMessage(msg.chat.id, "Wow, " + msg.left_chat_participant.first_name + " dropped out faster than Edwin did in physics.");
            }, 1400);
        }
    }

	if(msg.text.toLowerCase().contains("g/2")){
  		var opts = {
  			reply_to_message_id: msg.message_id,
		};
		bot.sendMessage(msg.chat.id, '*triggered*', opts);
	}
    else if(msg.text.substring(0, 6).toLowerCase().contains("tldr") || msg.text.substring(0, 6).toLowerCase().contains("tl;dr")){
        if(msg.chat.type === "group" && !msg.text.contains("/")){
            bot.sendMessage(msg.chat.id, "Want a TL;DR of the group chat? Message me /tldr last N (where N is the amount of messages): @WoodyardBot", {reply_to_message_id:msg.message_id});
        }
    }
    else if(msg.text === "Hi"){
        var opts = {
  			reply_to_message_id: msg.message_id,
		};
		bot.sendMessage(msg.chat.id, "Don't FUCKING say hi", opts);
    }
});


bot.onText(/\/echo (.+)/, function (msg, match) {
    if(refuse("echo", msg)){ return; }

    var chatId = msg.chat.id;
	var resp = match[1];
	bot.sendMessage(chatId, resp);
});

bot.onText(/\/speciallesson/, function(msg, match) {
    if(refuse("speciallesson", msg)){ return; }

	bot.forwardMessage(msg.chat.id, 1494633, 37);
});

bot.onText(/\/christmas/, function(msg, match){
    if(refuse("christmas", msg)){ return; }

    var christmas = new Date("Dec 25, 2016");
    var now = new Date();
	bot.sendMessage(msg.chat.id, "There are about " + (Math.floor((christmas.getTime() - now.getTime())/1000/60/60)) + " hours until Christmas! That's " + (christmas.getTime() - now.getTime()) + " milliseconds.", {reply_to_message_id:msg.message_id});
    setTimeout(function(){
        bot.forwardMessage(msg.chat.id, 1494633, 2902); //with every millisecond I become exponentially more excited
    }, 1000);
});

bot.onText(/\/countdownto (.+)/, function(msg, match){
    if(refuse("countdownto", msg)){ return; }

    var dateToCountdownTo = new Date(match[1]);
    if(dateToCountdownTo === undefined){
        bot.sendMessage(msg.chat.id, "Invalid format. Please enter in the following format: [Mo day, year]. For example:\n\nDec 25, 2015", {reply_to_message_id:msg.message_id});
        return;
    }
    var now = new Date();
    if(isNaN(Math.floor((dateToCountdownTo.getTime() - now.getTime())/1000))){
        bot.sendMessage(msg.chat.id, "Remove " + msg.from.first_name);
    }
    else{
	    bot.sendMessage(msg.chat.id, "There are about " + (Math.floor((dateToCountdownTo.getTime() - now.getTime())/1000/60/60)) + " hours until then! That's " + Math.floor((dateToCountdownTo.getTime() - now.getTime())/1000) + " seconds.", {reply_to_message_id:msg.message_id});
    }
});

var currentlyAskingForExamDate;

function provideExamDate(msg){
    var replyText = "Invalid period, sucker.";
    switch(msg.text.toLowerCase()){
        case "one":
        case "1":
            replyText = "Thursday, Jan 28, 2016 9:15";
            break;
        case "two":
        case "2":
            replyText = "Friday, Jan 29, 2016 9:15";
            break;
        case "three":
        case "3":
            replyText = "Monday, Feb 1, 2016 9:15";
            break;
        case "four":
        case "4":
            replyText = "Tuesday, Feb 2, 2016 9:15";
            break;
    }
    var d = new Date(replyText);
    var currentDate = new Date();
    var replyString = "Your exam will be on " + replyText + ".\n\nThat means you have aboot " + Math.floor((d.getTime() - currentDate.getTime())/1000/60/60) + " hours to study. (That's " + (d.getTime() - currentDate.getTime()) + " milliseconds.)";
    bot.sendMessage(msg.chat.id, replyString, { reply_to_message_id: msg.message_id });
}

bot.onText(/\/examdate/, function(msg, match){
    if(refuse("examdate", msg)){ return; }

    //currentlyTyping();

    var chatId = msg.chat.id;
    currentlyAskingForExamDate = msg.from.id;
    var opts = {
        reply_to_message_id: msg.message_id
    };
    bot.sendMessage(chatId, 'What period? (1-4)', opts);
});

bot.onText(/\/help/, function(msg, match){
    if(refuse("help", msg)){
        if(msg.forward_from){
            if(msg.forward_from.username === BOT_USERNAME){
                return;
            }
        }
        bot.sendMessage(msg.chat.id, "My creator has me currently disabled, sorry.");
        return;
    }

    if(!COMMAND_LIST_STATIC){
        console.log("Command list is not initialized yet, refusing.");
        bot.sendMessage(msg.chat.id, "Command failed. Please try again.", {reply_to_message_id:msg.message_id});
        return;
    }

    var commandList = COMMAND_LIST_DETAILS;
    var isGroupChat = (msg.chat.type === "group");
    var helpString = "";

    if(isGroupChat){
        helpString += "Here are all commands available for group chats.\n";
    }
    else{
        helpString += "Here are all commands available for private chats.\n";
    }

    console.log("Got " + commandList.commands.length);

    var helpString;
    for(index in commandList.commands){
        var commandObject = commandList.commands[index];
        console.log(JSON.stringify(commandObject));
        var objectString = "/" + commandObject.command.name + " ";
        if(commandObject.command.parameters){
            objectString += commandObject.command.parameters + " ";
        }
        objectString += "- ";
        objectString += commandObject.description;
        objectString += "\n";

        if((commandObject.group_enabled && isGroupChat) || (!isGroupChat)){
            if(!commandObject.isBanned){
                helpString += objectString;
            }
        }
    }

    console.log(helpString);
    bot.sendMessage(msg.chat.id, helpString, {reply_to_message_id:msg.message_id});
});

bot.onText(/\/define (.+)/, function(msg, match){
    if(refuse("define", msg)){ return; }
    var definition = urban(match[1]);

    definition.first(function(json) {
        if(json){
            bot.sendMessage(msg.chat.id, json.definition, { reply_to_message_id:msg.message_id });
        }
        else{
            bot.sendMessage(msg.chat.id, "Undefined, just like your penis", { reply_to_message_id:msg.message_id });
        }
    });
});

var AMOUNT_OF_WOMEN_AVAILABLE = 27;

bot.onText(/\/woman/, function(msg, match){
    if(refuse("woman", msg)){ return; }

    bot.sendChatAction(msg.chat.id, "upload_photo");

    var photoID = Math.floor(Math.random() * AMOUNT_OF_WOMEN_AVAILABLE);
    var photo = "women_bot/" + photoID + ".jpg";

	bot.sendPhoto(msg.chat.id, photo);
});

bot.onText(/\/source/, function(msg, match){
    if(refuse("source", msg)){ return; }

    bot.sendMessage(msg.chat.id, "Here's my source code: https://github.com/edwinfinch/telememebottesting/blob/master/WoodyardBot.js");
});

bot.onText(/\/flipacoin/, function(msg, match){
    if(refuse("flipacoin", msg)){ return; }

    var result = (Math.floor(Math.random() * 2));
    var rare = (Math.floor(Math.random() * 100));
    if(rare === 69){
        bot.sendMessage(msg.chat.id, "Sideways, try again", { reply_to_message_id:msg.message_id });
        return;
    }
    bot.sendMessage(msg.chat.id, (result == 1) ? "Heads" : "Tails", { reply_to_message_id: msg.message_id });
});

bot.onText(/\/lookup/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to lookup a user.", { reply_to_message_id:msg.message_id });
    }
    else{
        lookingUp = true;
        bot.sendMessage(msg.chat.id, "Please forward a message from the user you want to look up.", { reply_to_message_id:msg.message_id });
    }
});

bot.onText(/\/ban (.+)/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to ban a user from my classroom.");
    }
    else{
        bannedUsers[match[1]] = true;
        bot.sendMessage(msg.chat.id, match[1] + " has been banned from using me until further notice.");
    }
});

bot.onText(/\/unban (.+)/, function(msg, match){
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "Sorry, you don't have permission to unban a user from my classroom.");
    }
    else{
        bannedUsers[match[1]] = undefined;
        bot.sendMessage(msg.chat.id, match[1] + " has been unbanned and all commands are re-enabled.");
    }
});

bot.onText(/\/whatdoiown/, function(msg, match){
    if(refuse("whatdoiown", msg)){ return; }

    if(msg.chat.type !== "private"){
        bot.sendMessage(msg.chat.id, "To prevent spamming like Luka when he first used the bot, please only use this command in private chat: @WoodyardBot", {reply_to_message_id:msg.message_id});
        return;
    }
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if(err){
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", { reply_to_message_id: msg.message_id });
            return;
        }
        var jsonData = JSON.parse(data);
        var owns = [];
        for(key in jsonData){
            if(jsonData[key].owner === msg.from.id){
                owns.push(key);
            }
        }
        bot.sendMessage(msg.chat.id, "Please, DO NOT forward this message! If you do, you're just a dick, sorry. Thoughts are more fun when they are anonymous.\n\nYou own the following thoughts: " + owns, {reply_to_message_id:msg.message_id});
    });
});

bot.onText(/\/allthoughts/, function(msg, match){
    if(refuse("allthoughts", msg)){ return; }

    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "You can't do that, sorry", {reply_to_message_id:msg.message_id});
        return;
    }
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if(err){
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", { reply_to_message_id: msg.message_id });
            return;
        }
        var jsonData = JSON.parse(data);
        var owns = [];
        for(key in jsonData){
            owns.push(key);
        }
        bot.sendMessage(msg.chat.id, owns.length + " thoughts: " + owns, {reply_to_message_id:msg.message_id});
    });

});

bot.onText(/\/start/, function(msg, match){
    bot.sendMessage(msg.chat.id, "/stop", {reply_to_message_id:msg.message_id});
});

bot.onText(/\/stop/, function(msg, match){
    bot.sendMessage(msg.chat.id, "/start", {reply_to_message_id:msg.message_id});
});

bot.onText(/\/whoowns (.+)/, function(msg, match){
    if(refuse("whoowns", msg)){ return; }
/*
    if(msg.from.username !== "Lignite"){
        bot.sendMessage(msg.chat.id, "I'm sorry Mr. Ball, I'm afraid I can't let you do that.", {reply_to_message_id:msg.message_id});
        return;
    }
    */
    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if(err){
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", { reply_to_message_id: msg.message_id });
            return;
        }
        var jsonData = JSON.parse(data);
        var identity = match[1].toLowerCase();
        if(jsonData[identity] === undefined){
            bot.sendMessage(msg.chat.id, "Nobody has created this thought yet :O", { reply_to_message_id: msg.message_id });
            return;
        }
        console.log(JSON.stringify(jsonData) + " and " + jsonData[identity].ownerName);
        bot.sendMessage(msg.chat.id, "The owner of thought '" + identity + "' is " + jsonData[identity].ownerName + " (" + jsonData[identity].owner + ")", { reply_to_message_id: msg.message_id });
    });
});

bot.onText(/\/groupchat (.+)/, function(msg, match){ if(msg.from.username !== "Lignite"){ return; } bot.sendMessage(-12880731, match[1]); });

bot.onText(/\/removethought (.+)/, function(msg, match){
    if(refuse("removethought", msg)){ return; }

    currentlyTyping(msg);

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if(err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", { reply_to_message_id: msg.message_id });
            return;
        }

        var phrases = JSON.parse(data);
        var identity = match[1].toLowerCase();
        if(phrases[identity] !== undefined){
            if(phrases[identity].owner === msg.from.id || (msg.from.username === "Lignite")){
                console.log("Is from the same owner");
            }
            else{
                console.log("Is NOT from the same owner!!!");
                bot.sendMessage(msg.chat.id, "You didn't give me this thought so you can't delete it, sorry", { reply_to_message_id: msg.message_id });
                return;
            }
        }

        phrases[identity] = undefined;

        fs.writeFile("resources/thoughts.json", JSON.stringify(phrases), function(err) {
            if(err) {
                console.error("Could not write file: %s", err);
            }
            bot.sendMessage(msg.chat.id, "Thought deleted from my brain.", { reply_to_message_id: msg.message_id });
        });
    });
});

function addThought(msg, match){
    if(refuse("addthought", msg)){ return; }

    if(msg.chat.type !== "private"){
        bot.sendMessage(msg.chat.id, "Due to spamming and Dan whining, you can now only add thoughts in private chat: @WoodyardBot", {reply_to_message_id:msg.message_id});
        return;
    }

    currentlyTyping(msg);

    /*
    if(msg.chat.type === "group"){
        bot.sendMessage(msg.chat.id, "Sorry, you can't add thoughts through group chats. Please message me privately to add thoughts.", { reply_to_message_id: msg.message_id });
        return;
    }
    */

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        if(err) {
            console.error("Could not open file: %s", err);
            bot.sendMessage(msg.chat.id, "I had some trouble gathering my thoughts (" + err + ")", { reply_to_message_id: msg.message_id });
            return;
        }

        var identity = match[1].toLowerCase();

        if(!identity.contains(": ")){
            bot.sendMessage(msg.chat.id, "Sorry dawg, I can't add that thought. Please enter it in the correct format, for example:\n\n/addthought jaldeep: always lost", { reply_to_message_id: msg.message_id });
            return;
        }

        var firstColon = identity.indexOf(":");
        var name = identity.substring(0, firstColon);
        var phraseString = match[1].substring(firstColon+2);
        console.log("Name " + name + " and phrase " + phraseString);

        var phrases = JSON.parse(data);

        if(phrases[name] !== undefined){
            if(phrases[name].owner === msg.from.id){
                console.log("Is from the same owner");
            }
            else{
                console.log("Is NOT from the same owner!!!");
                bot.sendMessage(msg.chat.id, "You didn't give me this thought so you can't change it, sorry", { reply_to_message_id: msg.message_id });
                return;
            }
        }

        phrases[name] = {
            phrase:phraseString,
            owner:msg.from.id,
            ownerName:msg.from.first_name,
            timeAdded:msg.date
        };

        fs.writeFile("resources/thoughts.json", JSON.stringify(phrases), function(err) {
            if(err) {
                console.error("Could not write file: %s", err);
            }
            bot.sendMessage(msg.chat.id, "Thought engraved in my brain.", { reply_to_message_id: msg.message_id });
        });
    });
}

bot.onText(/\/at (.+)/, function(msg, match){
    addThought(msg, match);
});

bot.onText(/\/addthought (.+)/, function(msg, match){
    addThought(msg, match);
});

var NOT_SURE_MESSAGES = [
    "You know, I'm not really sure.", "They're pretty stupid", "What comes out of the south end of a north bound cow?",
    "Stop", "Jingle bell, Jingle bell", "Why the fuck do you want to know", "8/10 would bang again", "Part of the crew",
    "All good in my books, bad in my heart", "g/2", "2/g", "Not that hardcore of a memer", "Dropped out for a reason",
    "Don't trigger me on this nice day"
];

function thoughtsOn(msg, match){
    if(refuse("thoughtson", msg)){ return; }

    currentlyTyping(msg);

    fs.readFile("resources/thoughts.json", 'utf8', function(err, data) {
        var phrases = JSON.parse(data);
        setTimeout(function(){
            var identity = match[1].toLowerCase();
            if((phrases[identity] != undefined)){
                //console.log("Sending FUCKING SHIT");
                bot.sendMessage(msg.chat.id, phrases[identity].phrase, { reply_to_message_id: msg.message_id });
            }
            else{
                console.log("Sending backup");
                var toSend = NOT_SURE_MESSAGES[Math.floor(Math.random() * NOT_SURE_MESSAGES.length)];
                bot.sendMessage(msg.chat.id, toSend, { reply_to_message_id: msg.message_id });
            }
        }, 1000);
    });
}

bot.onText(/\/thoughton/, function(msg, match){
    thoughtsOn(msg, match);
});

bot.onText(/\/thoughtson (.+)/, function(msg, match){
    thoughtsOn(msg, match);
});

bot.onText(/\/to (.+)/, function(msg, match){
    thoughtsOn(msg, match);
});
