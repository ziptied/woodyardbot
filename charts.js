var request = require('request');
var analytics = require('./chat_analytics');
var fs = require('fs');
var plotly = require('plotly')('edwinfinch','zjoygft1ot');

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    //console.log('content-type:', res.headers['content-type']);
    //console.log('content-length:', res.headers['content-length']);
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

var sendGraph = function(bot, msg, reply){
    var downloadURL = reply.url + "/plot-title.png";
    download(downloadURL, 'image.png', function(){
        bot.sendPhoto(msg.chat.id, 'image.png');
    });
}

var renderHours = function(date, hoursArray, itemsArray, callback){
    var trace1 = {
        x: hoursArray,
        y: itemsArray,
        name: "Sent",
        type: "scatter"
    };
    var data = [trace1];
    var layout = {
        title: "Group Chat Messages on " + date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear(),
        font:{
            family: "Courier New, monospace",
            size: 20,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Hour",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    var graphOptions = {layout: layout, filename: "per-hour-graph", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        download(downloadURL, 'graphs/hours.png', function(){
            console.log("Downloaded new hours graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

var renderDays = function(date, daysArray, itemsArray, callback){
    var trace1 = {
        x: daysArray,
        y: itemsArray,
        name: "Sent",
        type: "scatter"
    };
    var data = [trace1];
    var date1 = new Date(date.getTime()-((86400*1000)*7));
    var layout = {
        title: "Group Chat from " + date1.getMonth() + "/" + date1.getDate() + "/" + date1.getFullYear() + " to " + date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear(),
        font:{
            family: "Courier New, monospace",
            size: 20,
            color: "#7f7f7f"
        },
        xaxis: {
            title: "Day",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        },
        yaxis: {
            title: "Amount Sent",
            titlefont: {
                family: "Courier New, monospace",
                size: 22,
                color: "#7f7f7f"
            }
        }
    };
    var graphOptions = {layout: layout, filename: "per-day-graph", fileopt: "overwrite"};
    plotly.plot(data, graphOptions, function (err, reply) {
        if(err){
            console.log(err);
            callback(err);
            return;
        }
        var downloadURL = reply.url + "/plot-title.png";
        console.log("Downloading from " + downloadURL);
        download(downloadURL, 'graphs/days.png', function(){
            console.log("Downloaded new days graph.");
            if(callback){
                callback(undefined);
            }
        });
    });
}

module.exports = {
    renderHoursChart: function(callback){
        var currentTime = Math.floor(Date.now()/1000);
        var date = new Date(currentTime*1000);

        var hours = date.getHours();
        var itemsArray = new Array(hours);
        var hoursArray = new Array(hours);
        console.log(hours);
        for(var i = 0; i < hours; i++){
            var greaterThan = currentTime-(3600*(i+1));
            var lessThan = currentTime-(3600*i);
            var location = i;
            analytics.getCountForSearch({ "message.date":{"$gt":greaterThan, "$lt":lessThan} }, location, function(data){
                itemsArray[data.location] = data.count;
                hoursArray[data.location] = hours-data.location;
            });
        }
        setTimeout(function(){
            var data = {
                items: itemsArray,
                hours: hoursArray
            }
            renderHours(date, hoursArray, itemsArray, callback);
        }, 1500);
    },
    renderDaysChart: function(callback){
        var currentTime = Math.floor(Date.now()/1000);
        var date = new Date(currentTime*1000);

        var daysBack = 7;
        var itemsArray = new Array(daysBack);
        var daysArray = new Array(daysBack);
        for(var i = 0; i < daysBack; i++){
            var greaterThan = currentTime-(86400*(i+1));
            var lessThan = currentTime-(86400*i);
            var location = i;
            analytics.getCountForSearch({ "message.date":{"$gt":greaterThan, "$lt":lessThan} }, location, function(data){
                itemsArray[data.location] = data.count;
                daysArray[data.location] = (new Date(date.getTime()-((86400*1000)*data.location))).getDate();
            });
        }
        setTimeout(function(){
            var data = {
                items: itemsArray,
                days: daysArray
            };
            renderDays(date, daysArray, itemsArray, callback);
        }, 1500);
    },
};
