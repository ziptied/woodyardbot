var mongojs = require('mongojs');
var db = mongojs('telegram_bot_analytics', ['group_chat_stats', 'group_chat_messages']);

module.exports = {
    writeToDatabase: function (stats, toAdd, callback) {
        db.group_chat_messages.insert(toAdd, function(error, docs){
            if(callback){
                callback(error, docs);
            }
        });
    },
    readFromDatabase: function (stats, toFind, callback) {
        db.group_chat_messages.find(toFind, function(err, docs){
            if(callback){
                callback(err, docs);
            }
        });
    },
    getTLDRData: function(totalMessages, howFarBack, callback){
        db.group_chat_messages.find({}).skip(totalMessages - howFarBack, function(err, docs){
            if(callback){
                callback(err, docs);
            }
        });
    },
    getUserStats: function(userID, callback){
        db.group_chat_messages.count({"message.from.id":userID}, function(err, count){
            if(err){
                console.log("Error getting stats: " + err);
                callback(undefined);
                return;
            }
            var stats = {
                sent: count
            };
            callback(stats);
        });
    },
    getTotalCount: function(callback){
        var count = db.group_chat_messages.count({}, function(err, count){
            if(err){
                console.log("Error " + err);
                callback(undefined);
            }
            callback(count);
        });
    },
    getCountForSearch: function(toFind, location, callback){
        var staticLocation = location;
        db.group_chat_messages.count(toFind, function(err, count){
            if(err){
                console.log("Error getting count: " + err);
                callback(undefined);
                return;
            }
            var stats = {
                count: count,
                location: staticLocation,
                found: toFind
            };
            callback(stats);
        });
    }
};
